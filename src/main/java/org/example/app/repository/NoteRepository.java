package org.example.app.repository;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.app.entity.NoteEntity;
import org.example.app.entity.UserEntity;
import org.example.template.JpaTransactionTemplate;

import java.time.Instant;
import java.util.List;

@Slf4j
@RequiredArgsConstructor
public class NoteRepository {
    final JpaTransactionTemplate template;

    public List<NoteEntity> getAll(final String name, final int limit, final int offset) {
        return template.executeInTransaction(em -> {
            final UserEntity userEntity = em.createQuery("SELECT e FROM UserEntity e WHERE e.login = :login", UserEntity.class)
                    .setParameter("login", name.toLowerCase()).getSingleResult();
            final List<NoteEntity> notes = em.createQuery("SELECT e FROM NoteEntity e WHERE e.userEntity.id = :userEntityId", NoteEntity.class)
                    .setParameter("userEntityId", userEntity.getId())
                    .setFirstResult(offset)
                    .setMaxResults(limit)
                    .getResultList();
            return notes;
        });
    }

    public NoteEntity create(final NoteEntity note, final String name) {
        template.executeInTransaction(em -> {
            em.persist(note);
            final UserEntity userEntity = em.createQuery("SELECT e FROM UserEntity e WHERE e.login = :login", UserEntity.class)
                    .setParameter("login", name.toLowerCase()).getSingleResult();
            note.setUserEntity(userEntity);
            em.merge(note);
            return note.getContent();
        });
        return note;
    }

    public NoteEntity getById(String noteId) {
        return template.executeInTransaction(em -> em.find(NoteEntity.class, Integer.parseInt(noteId)));

    }

    public void removeById(String noteId) {
        template.executeInTransaction(em -> {
            final NoteEntity reference = em.getReference(NoteEntity.class, Integer.parseInt(noteId));
            reference.setRemoved(true);
            em.merge(reference);
            return null;
        });
    }

    public NoteEntity update(NoteEntity note, String noteId) {
        return template.executeInTransaction(em -> {
            final NoteEntity noteEntity = em.find(NoteEntity.class, Integer.parseInt(noteId));
            noteEntity.setContent(note.getContent());
            noteEntity.setCreated(Instant.now());
            em.merge(noteEntity);
            return noteEntity;
        });
    }
}
