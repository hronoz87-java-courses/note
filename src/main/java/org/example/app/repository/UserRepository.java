package org.example.app.repository;

import org.example.app.entity.UserEntity;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.template.JpaTransactionTemplate;

import javax.persistence.TypedQuery;


@Slf4j
@RequiredArgsConstructor
public class UserRepository {
    final JpaTransactionTemplate template;

    public void save(final UserEntity user) {
        template.executeInTransaction(em -> {
            em.persist(user);
            return user;
        });
    }

    public UserEntity authenticateUser(final String login) {
        return template.executeInTransaction(em -> {
            final TypedQuery<UserEntity> query = em.createQuery("SELECT e FROM UserEntity e WHERE e.login = :login", UserEntity.class)
                    .setParameter("login", login.toLowerCase());
            final UserEntity userEntity = query.getSingleResult();
            log.debug("login: {}", userEntity);
            return userEntity;
        });
    }
}
