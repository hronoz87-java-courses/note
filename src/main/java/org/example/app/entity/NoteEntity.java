package org.example.app.entity;

import lombok.*;

import javax.persistence.*;
import java.time.Instant;

@Entity
@Table(name = "notes")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class NoteEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column( columnDefinition = "TEXT")
    private String content;
    @Basic(fetch = FetchType.LAZY)
    @ToString.Exclude
    private String mediaURI;

    @Column(nullable = false)
    private Boolean removed = false;
    @Column(updatable = false, columnDefinition = "timestamp DEFAULT CURRENT_TIMESTAMP")
    private Instant created = Instant.now();

    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserEntity userEntity;

}
