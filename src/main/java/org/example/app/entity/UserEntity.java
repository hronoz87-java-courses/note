package org.example.app.entity;

import lombok.*;

import javax.persistence.*;
import java.time.Instant;

@Entity
@Table(name = "users")
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class UserEntity {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;
  @Column(nullable = false, unique = true, columnDefinition = "TEXT")
  private String login;
  @Column(nullable = false, columnDefinition = "TEXT")
  private String password;
  @Column(nullable = false, columnDefinition = "TEXT")
  private String role = "ROLE_USER";

  @Column(nullable = false)
  private Boolean removed = false;
  @Column(updatable = false, columnDefinition = "timestamp DEFAULT CURRENT_TIMESTAMP")
  private Instant created = Instant.now();

}
