package org.example.app.dto.noteDTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class NoteDownloadMediaRS {
    private String mediaURI;
}
