package org.example.app.dto.noteDTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class NoteCreateRS {
    private int id;
    private String content;
    private String mediaURI;
}
