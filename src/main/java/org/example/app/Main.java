package org.example.app;

import org.example.app.controller.NoteController;
import org.example.app.controller.UserController;
import org.example.app.repository.NoteRepository;
import org.example.app.repository.UserRepository;
import org.example.app.service.NoteService;
import org.example.app.service.UserService;
import com.google.gson.Gson;
import example.org.processor.AuditBeanProcessor;
import example.org.processor.HasRoleBeanProcessor;
import lombok.extern.slf4j.Slf4j;
import org.example.framework.di.Container;
import org.example.framework.form.FormParser;
import org.example.framework.http.HttpMethods;
import org.example.framework.http.Server;
import org.example.framework.multipart.MultiPartParser;
import org.example.framework.query.QueryParser;
import org.example.middleware.AnonAuthMiddleware;
import org.example.middleware.BasicAuthNMiddleware;

import org.example.template.JpaTransactionTemplate;
import org.example.util.Maps;
import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.io.IOException;
import java.util.regex.Pattern;

@Slf4j
public class Main {
    public static void main(String[] args) throws Throwable {

        System.setProperty("javax.net.ssl.keyStore", "web-certs/server.jks");
        System.setProperty("javax.net.ssl.keyStorePassword", "passphrase");

        final Container container = new Container();
        final EntityManagerFactory emf = Persistence.createEntityManagerFactory("default");

        container.register("auditBeanProcessor", AuditBeanProcessor.class);
        container.register("hasRoleBeanProcessor", HasRoleBeanProcessor.class);
        container.register(Gson.class);
        container.register(new Argon2PasswordEncoder());
        container.register("UserService", UserService.class);
        container.register("UserController", UserController.class);
        container.register("UserRepository", UserRepository.class);
        container.register("NoteController", NoteController.class);
        container.register("NoteService", NoteService.class);
        container.register("NoteRepository", NoteRepository.class);
        container.register(new JpaTransactionTemplate(emf));

        container.wire();

        final UserService userService = container.getBean("UserService", UserService.class);
        final UserController userController = container.getBean("UserController", UserController.class);
        final NoteController noteController = container.getBean("NoteController", NoteController.class);

        final Server server = Server.builder()
                .middleware(new BasicAuthNMiddleware(userService))
                .middleware(new AnonAuthMiddleware())
                .parser(new QueryParser())
                .parser(new FormParser())
                .parser(new MultiPartParser())
                .routes(
                        Maps.of(
                                Pattern.compile("^/users$"), Maps.of(
                                        HttpMethods.POST, userController::register
                                ),
                                Pattern.compile("^/note$"), Maps.of(
                                        HttpMethods.GET, noteController::getAll,
                                        HttpMethods.POST, noteController::createNote
                                ),
                                Pattern.compile("^/note/download$"), Maps.of(
                                        HttpMethods.POST, noteController::downloadMedia
                                ),
                                Pattern.compile("^/note/(?<noteId>[0-9]+)$"),
                                Maps.of(
                                        HttpMethods.GET, noteController::getById,
                                        HttpMethods.DELETE, noteController::deleteById,
                                        HttpMethods.PATCH, noteController::updateNote

                                )
                        )
                )
                .build();

        try {
            server.serveHttps(9999);
            Thread.sleep(100_000);
//            server.stop();
//        } catch (IOException e) {
//            log.error("can't serve", e);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}


