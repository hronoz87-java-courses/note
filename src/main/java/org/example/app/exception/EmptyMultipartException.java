package org.example.app.exception;

public class EmptyMultipartException extends RuntimeException{
    public EmptyMultipartException() {
    }

    public EmptyMultipartException(String message) {
        super(message);
    }

    public EmptyMultipartException(String message, Throwable cause) {
        super(message, cause);
    }

    public EmptyMultipartException(Throwable cause) {
        super(cause);
    }

    public EmptyMultipartException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
