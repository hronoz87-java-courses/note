package org.example.app.exception;

public class LoginAlreadyRegisteredException extends RuntimeException{
    public LoginAlreadyRegisteredException() {
    }

    public LoginAlreadyRegisteredException(String message) {
        super(message);
    }

    public LoginAlreadyRegisteredException(String message, Throwable cause) {
        super(message, cause);
    }

    public LoginAlreadyRegisteredException(Throwable cause) {
        super(cause);
    }

    public LoginAlreadyRegisteredException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
