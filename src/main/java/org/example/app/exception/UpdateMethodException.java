package org.example.app.exception;

public class UpdateMethodException extends RuntimeException{
    public UpdateMethodException() {
    }

    public UpdateMethodException(String message) {
        super(message);
    }

    public UpdateMethodException(String message, Throwable cause) {
        super(message, cause);
    }

    public UpdateMethodException(Throwable cause) {
        super(cause);
    }

    public UpdateMethodException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
