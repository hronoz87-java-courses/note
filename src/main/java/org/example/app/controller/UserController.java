package org.example.app.controller;

import org.example.app.dto.userDTO.UserRegisterRQ;
import org.example.app.dto.userDTO.UserRegisterRS;
import org.example.app.service.UserService;
import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.framework.http.Request;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

@Slf4j
@RequiredArgsConstructor
public class UserController {
    private final Gson gson;
    private final UserService service;

    public void register(final Request request, final OutputStream responseStream) throws IOException {
        final String requestBody = new String(request.getBody(), StandardCharsets.UTF_8);
        final UserRegisterRQ requestDTO = gson.fromJson(requestBody, UserRegisterRQ.class);
        final UserRegisterRS responseDTO = service.create(requestDTO);
        final byte[] responseBody = gson.toJson(responseDTO).getBytes(StandardCharsets.UTF_8);
        writeResponse(responseStream, responseBody);
    }

    private void writeResponse(OutputStream responseStream, byte[] body) throws IOException {
        responseStream.write((
                "HTTP/1.1 200 Ok\r\n" +
                        "Content-Length: " + body.length + "\r\n" +
                        "Connection: close\r\n" +
                        "Content-Type: text/plain\r\n" +
                        "\r\n"
        ).getBytes(StandardCharsets.UTF_8));
        responseStream.write(body);
    }
}
