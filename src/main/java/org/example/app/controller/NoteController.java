package org.example.app.controller;

import org.example.app.dto.noteDTO.*;
import org.example.app.service.NoteService;
import com.google.gson.Gson;
import example.org.annotation.Audit;
import example.org.annotation.HasRole;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.framework.http.Request;
import org.example.framework.multipart.Part;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.Map;

@Slf4j
@RequiredArgsConstructor
public class NoteController {
    private final Gson gson;
    private final NoteService service;

    @Audit
    public void getAll(final Request request, final OutputStream responseStream) throws IOException {
        final List<NoteGetAllRS> responseDTO = service.getAll();
        final byte[] responseBody = gson.toJson(responseDTO).getBytes(StandardCharsets.UTF_8);
        writeResponse(responseStream, responseBody);
    }

    @Audit
    @HasRole("ROLE_USER")
    public void getById(final Request request, final OutputStream responseStream) throws IOException {
        final String noteId = request.getPathGroup("noteId");
        final NoteGetByIdRS byId = service.getById(noteId);
        final byte[] responseBody = gson.toJson(byId).getBytes(StandardCharsets.UTF_8);
        writeResponse(responseStream, responseBody);
    }

    public void createNote(final Request request, final OutputStream responseStream) throws IOException {
        final String requestBody = new String(request.getBody(), StandardCharsets.UTF_8);
        final NoteCreateRQ requestDTO = gson.fromJson(requestBody, NoteCreateRQ.class);
        final NoteCreateRS responseDTO = service.create(requestDTO);
        final byte[] responseBody = gson.toJson(responseDTO).getBytes(StandardCharsets.UTF_8);
        writeResponse(responseStream, responseBody);
    }

    public void downloadMedia(final Request request, final OutputStream responseStream) throws IOException {
        final Map<String, List<Part>> multipartPart = request.getMultipartParts();
        final NoteDownloadMediaRS responseDTO = service.downloadImage(multipartPart);
        final byte[] responseBody = gson.toJson(responseDTO).getBytes(StandardCharsets.UTF_8);
        writeResponse(responseStream, responseBody);
    }

    public void deleteById(final Request request, final OutputStream responseStream) throws IOException {
        final String noteId = request.getPathGroup("noteId");
        final boolean byId = service.removeById(noteId);
        final byte[] body = ("flat with id: " + noteId + " removed " + byId).getBytes(StandardCharsets.UTF_8);
        writeResponse(responseStream, body);
    }

    public void updateNote(final Request request, final OutputStream responseStream) throws IOException {
        final String requestBody = new String(request.getBody(), StandardCharsets.UTF_8);
        final NoteUpdateRQ requestDTO = gson.fromJson(requestBody, NoteUpdateRQ.class);
        final String noteId = request.getPathGroup("noteId");
        final NoteUpdateRS responseDTO = service.update(requestDTO, noteId);
        final byte[] responseBody = gson.toJson(responseDTO).getBytes(StandardCharsets.UTF_8);
        writeResponse(responseStream, responseBody);
    }

    private void writeResponse(OutputStream responseStream, byte[] body) throws IOException {
        responseStream.write((
                "HTTP/1.1 200 Ok\r\n" +
                        "Content-Length: " + body.length + "\r\n" +
                        "Connection: close\r\n" +
                        "Content-Type: text/plain\r\n" +
                        "\r\n"
        ).getBytes(StandardCharsets.UTF_8));
        responseStream.write(body);
    }
}
