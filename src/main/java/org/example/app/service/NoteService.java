package org.example.app.service;

import org.example.app.dto.noteDTO.*;
import org.example.app.entity.NoteEntity;
import org.example.app.exception.EmptyMultipartException;
import org.example.app.repository.NoteRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.framework.auth.SecurityContext;
import org.example.framework.exception.AccessDeniedException;
import org.example.framework.multipart.FilePart;
import org.example.framework.multipart.Part;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.Principal;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@RequiredArgsConstructor
public class NoteService {
    private final NoteRepository repository;
    private static final int SEARCH_SIZE_LIMIT = 50;
    private static final int SEARCH_START = 0;
    private static final Path DIRECTORIES = Paths.get("media");

    public List<NoteGetAllRS> getAll() {
        final Principal principal = SecurityContext.getPrincipal();
        if (principal.getName().equals("ANONYMOUS")) {
            throw new AccessDeniedException("Дотуп запрещен");
        }
        final List<NoteEntity> allNotes = repository.getAll(principal.getName(), SEARCH_SIZE_LIMIT, SEARCH_START);
        return allNotes.stream().map(o -> new NoteGetAllRS(
                o.getId(),
                o.getContent(),
                o.getMediaURI()
        )).collect(Collectors.toList());
    }

    public NoteCreateRS create(final NoteCreateRQ requestDTO) {
        final Principal principal = SecurityContext.getPrincipal();
        if (principal.getName().equals("ANONYMOUS")) {
            throw new AccessDeniedException("Дотуп запрещен");
        }
        final NoteEntity note = new NoteEntity();
        note.setContent(requestDTO.getContent());
        note.setMediaURI(requestDTO.getMediaURI());
        final NoteEntity noteEntity = repository.create(note, principal.getName());
        final NoteCreateRS noteCreateRS = new NoteCreateRS();
        noteCreateRS.setId(noteEntity.getId());
        noteCreateRS.setContent(noteEntity.getContent());
        noteCreateRS.setMediaURI(noteEntity.getMediaURI());
        return noteCreateRS;
    }

    public NoteGetByIdRS getById(final String noteId) {
        final Principal principal = SecurityContext.getPrincipal();
        if (principal.getName().equals("ANONYMOUS")) {
            throw new AccessDeniedException("Дотуп запрещен");
        }
        final NoteEntity notesById = repository.getById(noteId);
        return new NoteGetByIdRS(
                notesById.getId(),
                notesById.getContent(),
                notesById.getMediaURI());
    }

    public boolean removeById(final String noteId) {
        final Principal principal = SecurityContext.getPrincipal();
        if (principal.getName().equals("ANONYMOUS")) {
            throw new AccessDeniedException("Дотуп запрещен");
        }
        repository.removeById(noteId);
        return true;
    }

    public NoteUpdateRS update(final NoteUpdateRQ requestDTO, final String noteId) {
        final Principal principal = SecurityContext.getPrincipal();
        if (principal.getName().equals("ANONYMOUS")) {
            throw new AccessDeniedException("Дотуп запрещен");
        }
        final NoteEntity note = new NoteEntity();
        note.setContent(requestDTO.getContent());
        final NoteEntity noteEntity = repository.update(note, noteId);
        final NoteUpdateRS noteUpdateRS = new NoteUpdateRS();
        noteUpdateRS.setId(noteEntity.getId());
        noteUpdateRS.setContent(noteEntity.getContent());
        noteUpdateRS.setMedia(noteUpdateRS.getMedia());
        return noteUpdateRS;
    }

    public NoteDownloadMediaRS downloadImage(final Map<String, List<Part>> multipartPart) throws IOException {
        final Principal principal = SecurityContext.getPrincipal();
        if (principal.getName().equals("ANONYMOUS")) {
            throw new AccessDeniedException("Дотуп запрещен");
        }
        final Path directories = Files.createDirectories(DIRECTORIES);
        final Collection<List<Part>> values = multipartPart.values();
        final List<Part> value = values.stream().findFirst().orElseThrow(EmptyMultipartException::new);
        final FilePart part = ((FilePart) value.get(0));
        File file = new File(String.valueOf(directories), UUID.randomUUID() + part.getFileName());
        writeFile(file, part);
        final NoteDownloadMediaRS noteDownloadMediaRS = new NoteDownloadMediaRS();
        noteDownloadMediaRS.setMediaURI(file.getPath());
        return noteDownloadMediaRS;
    }

    private void writeFile(final File file, final FilePart part) {
        try (final FileOutputStream outputStream = new FileOutputStream(file)) {
            outputStream.write(part.getContent());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}