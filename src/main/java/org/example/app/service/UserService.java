package org.example.app.service;

import org.example.app.dto.userDTO.UserRegisterRQ;
import org.example.app.dto.userDTO.UserRegisterRS;
import org.example.app.entity.UserEntity;
import org.example.app.repository.UserRepository;
import example.org.annotation.Audit;
import example.org.security.Auth;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.example.framework.auth.AuthenticationToken;
import org.example.framework.auth.Authenticator;
import org.example.framework.auth.LoginPasswordAuthenticationToken;
import example.org.auth.SecurityContext;
import org.example.framework.exception.UnsupportedAuthenticationToken;
import org.springframework.security.crypto.password.PasswordEncoder;


@Slf4j
@RequiredArgsConstructor
public class UserService implements Authenticator {
    private final UserRepository userRepository;
    private final PasswordEncoder encoder;

    @Audit
    public UserRegisterRS create(final UserRegisterRQ requestDTO) {
        final String login = requestDTO.getLogin().trim().toLowerCase();
        final String encodedPassword = encoder.encode(requestDTO.getPassword());
        synchronized (this) {
            final UserEntity user = new UserEntity();
            user.setLogin(login);
            user.setPassword(encodedPassword);
            userRepository.save(user);
            return new UserRegisterRS(login);
        }
    }

    @Override
    public boolean authenticate(final AuthenticationToken request) {
        if (!(request instanceof LoginPasswordAuthenticationToken)) {
            throw new UnsupportedAuthenticationToken();
        }

        final LoginPasswordAuthenticationToken converted = (LoginPasswordAuthenticationToken) request;
        final String login = converted.getLogin();
        final String password = (String) converted.getCredentials();

        final String encodedPassword;

        synchronized (this) {
            if (SecurityContext.getPrincipal() == null) {
                final UserEntity userEntity = userRepository.authenticateUser(login);
                SecurityContext.setAuth(Auth.builder().name(userEntity.getLogin()).role(userEntity.getRole()).build());
                return false;
            }
            encodedPassword = SecurityContext.getPrincipal().getName();
        }
        return encoder.matches(password, encodedPassword);

    }
}
